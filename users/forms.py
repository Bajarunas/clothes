from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm


class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def __init__(self, *args, **kwargs):
        super(NewUserForm, self).__init__(*args, **kwargs)
        form_fields = [{'form_name': 'username', 'placeholder': 'Username'},
                       {'form_name': 'email', 'placeholder': 'Email'},
                       {'form_name': 'password1', 'placeholder': 'Password'},
                       {'form_name': 'password2', 'placeholder': 'Repeat Password'}]
        for field in form_fields:
            self.fields[field.get('form_name')].widget.attrs.update(
                {'class': 'form-control', 'placeholder': (field.get('placeholder'))})

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user


class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username', 'password']

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        form_fields = [{'form_name': 'username', 'placeholder': 'Username'},
                {'form_name': 'password', 'placeholder': 'Password'}]
        for field in form_fields:
            self.fields[field.get('form_name')].widget.attrs.update(
                {'class': 'form-control', 'placeholder': (field.get('placeholder'))})
