from django.contrib import admin
from .models import Brand, Color, Sort, Gender, Size, Clothe

myModels = [Brand, Color, Sort, Gender, Size, Clothe]

admin.site.register(myModels)
