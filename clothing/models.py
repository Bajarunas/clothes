from django.db import models


class Brand(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()


class Color(models.Model):
    title = models.CharField(max_length=50)


class Sort(models.Model):
    title = models.CharField(max_length=50)


class Gender(models.Model):
    title = models.CharField(max_length=50)


class Size(models.Model):
    title = models.CharField(max_length=50)


class Clothe(models.Model):
    title = models.CharField(max_length=50)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    description = models.TextField()
    color = models.ForeignKey(Color, on_delete=models.CASCADE)
    size = models.ForeignKey(Size, on_delete=models.CASCADE)
    sort = models.ForeignKey(Sort, on_delete=models.CASCADE)
    gender = models.ForeignKey(Gender, on_delete=models.CASCADE)
    size = models.ForeignKey(Size, on_delete=models.CASCADE)
    cost = models.FloatField()
    review = models.IntegerField()
