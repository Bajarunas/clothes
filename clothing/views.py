from django.shortcuts import render


def home(request):
    return render(request=request, template_name='home.html')


def contact(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        address = request.POST.get('address')
        message = request.POST.get('message')
        print('Email:', email, 'Phone:', phone, 'Address:', address, 'Message:', message)
    return render(request=request, template_name='contact.html')
