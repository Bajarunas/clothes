# Project Clothing
#### Created By Liudvikas Bajarūnas


## How to run the project:

#### 1. Clone the project.

`git clone https://gitlab.com/Bajarunas/clothes.git`

#### 2. Go to cloned folder and install the requirements.

`cd clothes`
`pip3 install -r requirements.txt`

#### 3. Run the server.

`python3 manage.py runserver`